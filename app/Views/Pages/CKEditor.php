<?= $this->extend('admin/layout/template.php', $data); ?>

<?= $this->section('content'); ?>
<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?= $this->include('admin/layout/sidebar'); ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

    <div id="editor">This is some sample content.</div>
                <script>
                        ClassicEditor
                                .create( document.querySelector( '#editor' ) )
                                .then( editor => {
                                        console.log( editor );
                                } )
                                .catch( error => {
                                        console.error( error );
                                } );
                </script>

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <?= $this->include('admin/layout/footer'); ?>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<script>
     var jsPDF = window.jspdf.jsPDF
        const doc = new jsPDF(
            unit: "pt",
            orientation: "p",
            lineHeight: 1.2
        )

    function JSONtoPDF(params) {
        // JSON to PDF
        doc.text('Hello world!', 10, 10);
        doc.save('a4.pdf');
    }

</script>

<?= $this->endsection(); ?>