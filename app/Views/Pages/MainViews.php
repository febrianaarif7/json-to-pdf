<?= $this->extend('admin/layout/template.php', $data); ?>

<?= $this->section('content'); ?>
<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <?= $this->include('admin/layout/sidebar'); ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <?= $this->include('admin/layout/topbar'); ?>
            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">JSON to PDF Report Generator.</h1>
                </div>

                <div class="row">
                </div>

                <div class="row">

                    <div class="col-lg-6">

                        <div class="card mb-4">
                            <div class="card-header">
                                <h4 class="small font-weight-bold">Images</span></h4>
                            </div>
                            <div class="card-body">
                                <input type="file" onchange="readURL(this);" id="myImages" name="MyImages" accept="image/png, image/gif, image/jpeg" />
                            </div>
                        </div>

                        <div class="card mb-4">
                            <div class="card-header">
                                <h4 class="small font-weight-bold">JSON Text</span></h4>
                            </div>
                            <div class="card-body">
                                <textarea class="form-control" onfocusout="isValid();" id="TextAreaJSON" rows="3"></textarea>
                            </div>

                            <div class="card-body">
                                <h5 class="small font-weight-bold">Font Family & Font Size : </span></h5>
                                <select onchange="isValid();" name="SetFontFamily" id="setFontFamily">
                                    <option value="courier" selected>Courier</option>
                                    <option value="helvetica">Helvetica</option>
                                    <option value="times">Times</option>
                                </select> |
                                <select onchange="isValid();" name="SetFontSize" id="setFontSize">
                                    <option value="16" selected>16pt.</option>
                                    <option value="14">14pt.</option>
                                    <option value="10">10pt.</option>
                                </select>
                            </div>

                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="card mb-4">
                            <div class="card-header">
                                <h4 class="small font-weight-bold">PDF Viewer</span></h4>
                            </div>
                            <style>
                                embed,
                                iframe,
                                object {
                                    margin: 0 !important;
                                    border: 0;
                                    width: 35vw;
                                    height: 60vh;
                                }
                            </style>
                            <div class="card-body">
                                <center>
                                    <embed id="pdfEmbed" src="" filename="JSONtoPDF.pdf" type="application/pdf" />
                                </center>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <?= $this->include('admin/layout/footer'); ?>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<script>
    var jsPDF = window.jspdf.jsPDF
    let doc = new jsPDF();
    let imgURL = "";
    // doc.output('dataurlnewwindow');

    function isValid() {
        var textArea = document.getElementById("TextAreaJSON").value;
        var fontFamily = document.getElementById("setFontFamily").value;
        var fontSize = document.getElementById("setFontSize").value;
        var inputImages = document.getElementById("myImages");

        // console.log(imgURL);

        if (textArea.trim() != "") {
            var isJson = isJsonString(textArea);
            if (!isJson) {
                return (0);
            }
            JSONtoPDF(fontFamily, fontSize, textArea);
        }
    }

    function isJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            alert("Unknown text format");
            //document.getElementById("TextAreaJSON").value = "";
            return false;
        }
        return true;
    }

    function JSONtoPDF(fontFamily, fontSize, textArea) {

        
        // JSON to PDF    
        let doc = new jsPDF({
            orientation: 'p',
            unit: 'mm',
            format: 'a4',
            putOnlyUsedFonts: true
        });

        let pageSize = doc.internal.pageSize;
        let pageWidth = pageSize.width ? pageSize.width : pageSize.getWidth();
        let pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
        let pageNumX = pageWidth - 10;
        let pageNumY = pageHeight - 10;

        let nLines = 0;
        let strChar = "";
        let textWidth;

        let embedPDF = document.getElementById("pdfEmbed");
        let data = JSON.parse(textArea);
        let objEntries = Object.keys(data["report"]);
        let xAlign;

        console.log(data["report"]);

        doc.setFont(fontFamily);

        // Date Documents
        if ('date' in data["report"]) {
            doc.setFontSize(8);
            let objEntDate = Object.keys(data["report"]["title"]);
            strChar = 'Date: ' + data["report"]["date"];
            textWidth = doc.getTextWidth(strChar);
            xAlign = doc.internal.pageSize.getWidth() - textWidth - 10; // RIGHT
            doc.text(strChar, xAlign, 5);
            console.log('Date');
        }

        // Title        
        if ('title' in data["report"]) {
            doc.setFontSize(24);
            nLines = nLines + 20;
            let objEntTitle = Object.keys(data["report"]["title"]);
            strChar = data["report"]["title"];
            textWidth = doc.getTextWidth(strChar);
            xAlign = (doc.internal.pageSize.getWidth() - textWidth) / 2;
            doc.text(strChar, xAlign, nLines); // Title CENTRE

            doc.setProperties({
                title: strChar,
                subject: 'Info about PDF',
                author: 'Arif Febriana',
                keywords: 'generated, javascript, jsPDF, synergyFA',
                creator: 'Synergy FA'
            });
            console.log('Title');
        }

        // Images
        if (imgURL.trim() != "") {
            nLines = nLines + 20;
            doc.addImage(imgURL, 'JPEG', 15, nLines - 10, 30, 30);
            nLines = nLines + 5;
        } else {
            nLines = nLines + 25;
        }

        if ('client' in data["report"]) {
            // Clients
            doc.setFontSize(18);

            if ('name' in data["report"]["client"]) {
                nLines = 45;
                strChar = data["report"]["client"]["name"];
                doc.text(strChar, 55, nLines);
                nLines = nLines + 10; // Break Line
            }

            if ('account_number' in data["report"]["client"]) {

                strChar = data["report"]["client"]["account_number"];
                doc.text(strChar, 55, nLines);
                nLines = nLines + 10; // Break Line
            }

            if ('contact' in data["report"]["client"]) {
                doc.setFontSize(14);
                strChar = "Contact";
                nLines = 45;
                doc.text(strChar, 55, nLines);
                nLines = nLines + 10; // Break Line  

                if ('name' in data["report"]["client"]["contact"]) {
                    doc.setFontSize(12);
                    strChar = "Full Name : " + data["report"]["client"]["contact"]["name"];
                    doc.text(strChar, 55, nLines);
                    nLines = nLines + 10; // Break Line    
                }

                if ('email' in data["report"]["client"]["contact"]) {
                    doc.setFontSize(12);
                    strChar = "Email : " + data["report"]["client"]["contact"]["email"];
                    doc.text(strChar, 55, nLines);
                    nLines = nLines + 10; // Break Line   
                }

                if ('phone' in data["report"]["client"]["contact"]) {
                    doc.setFontSize(12);
                    strChar = "Phone : " + data["report"]["client"]["contact"]["phone"];
                    doc.text(strChar, 55, nLines);
                    nLines = nLines + 10; // Break Line  
                }

                if ('address' in data["report"]["client"]["contact"]) {

                    strChar = "Address : ";
                    doc.text(strChar, 55, nLines);
                    nLines = nLines + 10; // Break Line  

                    if ('street' in data["report"]["client"]["contact"]["address"]) {
                        strChar = "Street : " + data["report"]["client"]["contact"]["address"]["street"];
                        doc.text(strChar, 55, nLines);
                        nLines = nLines + 10; // Break Line  
                    }
                    if ('city' in data["report"]["client"]["contact"]["address"]) {
                        strChar = "City : " + data["report"]["client"]["contact"]["address"]["city"];
                        doc.text(strChar, 55, nLines);
                        nLines = nLines + 10; // Break Line  
                    }
                    if ('state' in data["report"]["client"]["contact"]["address"]) {
                        strChar = "State : " + data["report"]["client"]["contact"]["address"]["state"];
                        doc.text(strChar, 55, nLines);
                        nLines = nLines + 10; // Break Line  
                    }

                    if ('zip' in data["report"]["client"]["contact"]["address"]) {
                        strChar = "Zip : " + data["report"]["client"]["contact"]["address"]["zip"];
                        doc.text(strChar, 55, nLines);
                        nLines = nLines + 10; // Break Line  
                    }
                }
            }
            console.log('Client');
        }

        if ('pages' in data["report"]) {
            let nCountPages = data.report.pages.length;

            data["report"]["pages"].forEach(element => {

                doc.addPage();
                nLines = 0;

                if ('data' in element) {

                    const options = {
                        margin: {
                            top: 60
                        },
                        headStyles: {
                            fillColor: [41, 128, 185]
                        },
                        bodyStyles: {
                            textColor: [0, 0, 255]
                        },
                        theme: 'striped'
                    };

                    if (Array.isArray(element.data)) {
                        console.log(element.data);

                        let columns = Object.keys(element["data"][0]);
                        let dataDT = element.data.map(obj => Object.values(obj));

                        console.log(dataDT);

                        doc.autoTable({
                            head: [columns],
                            body: dataDT,
                            options: options
                        });
                    }

                    if (!Array.isArray(element.data)) {
                        console.log(element.data);

                        if (!Array.isArray(element.data)) {
                            let columns = Object.keys(element["data"]);
                            doc.autoTable({
                                head: [columns],
                                body: Object.values(element["data"]),
                                options: options
                            });
                        }
                    }
                }

                if ('summary' in element) {

                    nLines = 20;
                    strChar = "Recommendations : ";
                    doc.text(strChar, 20, nLines);
                    nLines = nLines + 10; // Break Line  

                    nLines = 20;
                    strChar = "";
                    doc.text(strChar, 20, nLines);
                    nLines = nLines + 10; // Break Line 

                    doc.setFontSize(12);
                    strChar = element["summary"]["recommendations"];
                    let lines = doc.splitTextToSize(strChar, 180);
                    doc.text(20, nLines, lines);
                }
            });
        }

        embedPDF.src = doc.output('datauristring', 'lala.pdf');
        // doc.save('a4.pdf');
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                imgURL = e.target.result;
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<?= $this->endsection(); ?>