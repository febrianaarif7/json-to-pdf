<?php 
namespace App\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\Exceptions\PageNotFoundException; // Add this line

class CoreController extends BaseController{
    public function index()
    {
        $d = ['data' => 'Cards'];
        return view('Pages/MainViews', $d);
    }
}